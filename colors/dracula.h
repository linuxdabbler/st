/* Terminal colors (16 first used in esacpe sequence) */
static const char *colorname[] = {
/* 8 normal colors */
    "#000000", /* black   */
    "#ff5555", /* red     */
    "#50fa7b", /* green   */
    "#f1fa8c", /* yellow  */
    "#bd93f9", /* blue    */
    "#ff79c6", /* magenta */
    "#8be9fd", /* cyan    */
    "#bbbbbb", /* white   */

    /* 8 bright colors */
    "#44475a", /* black   */
    "#ff5555", /* red     */
    "#50fa7b", /* green   */
    "#f1fa8c", /* yellow  */
    "#bd93f9", /* blue    */
    "#ff79c6", /* magenta */
    "#8be9fd", /* cyan    */
    "#ffffff", /* white   */

    [255] = 0,

    /* more colors can be added after 255 to use with DefaultXX */
    "#f1fa8c", /* 256 cursor */
    "#666666", /* 257 rev cursor */
    "#282a36", /* 258 bg */
    "#f8f8f2", /* 259 fg */
};

/* default colors (colorname index )*/
/* foreground, background, cursor, reverse cursor */
unsigned int defaultfg = 259;
unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;

