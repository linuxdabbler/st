/* Terminal colors (16 first used in esacpe sequence) */
static const char *colorname[] = {
/* 8 normal colors */
    "#000000", /* black   */
    "#be5046", /* red     */
    "#98c379", /* green   */
    "#d19a66", /* yellow  */
    "#61afef", /* blue    */
    "#c678dd", /* magenta */
    "#56b6c2", /* cyan    */
    "#abb2bf", /* white   */

    /* 8 bright colors */
    "#282c34", /* black   */
    "#e06c75", /* red     */
    "#98c379", /* green   */
    "#e5c07b", /* yellow  */
    "#61afef", /* blue    */
    "#c678dd", /* magenta */
    "#56b6c2", /* cyan    */
    "#ffffff", /* white   */

    [255] = 0,

    /* more colors can be added after 255 to use with DefaultXX */
    "#abb2bf", /* 256 cursor */
    "#666666", /* 257 rev cursor */
    "#282c34", /* 258 bg */
    "#abb2bf", /* 259 fg */
};

/* default colors (colorname index )*/
/* foreground, background, cursor, reverse cursor */
unsigned int defaultfg = 259;
unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;

