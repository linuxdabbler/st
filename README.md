# st

#### my updated build of st with theming and a few patches

---

This build of st was, at first, forked from Luke Smith, but later built from scratch
with alpha patch, copyurl, and scrollback.  Now another feature has been added recently.
a colors folder has been added to contain colorschemes for st and an #include line has
been added to config.def.h to easily theme st.  simply edit the #include line in
config.def.h, rm config.h, run make, run st, and the new theme will be applied to all
future terminal spawns.

---


